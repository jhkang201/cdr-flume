package dev.jhk.cdr.flume;

import org.apache.flume.serialization.*;
import com.google.common.collect.Lists;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.annotations.InterfaceAudience;
import org.apache.flume.annotations.InterfaceStability;
import org.apache.flume.event.EventBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.commons.io.output.ByteArrayOutputStream;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

/**
 * A deserializer that parses UDR Records from a file.
 */
@InterfaceAudience.Private
@InterfaceStability.Evolving
public class ByteUDRDeserializer implements EventDeserializer {

  private static final Logger logger = LoggerFactory.getLogger(ByteUDRDeserializer.class);

  private final ResettableInputStream in;
  private final Charset outputCharset;
  private final int packetType;
  private final int headPacketLength;
  private final int bodyPacketLength;
  private volatile boolean isOpen;
  private byte[] byteHeader = null;
  private byte[] byteBody = null;
  private byte[] byteEvent = null;
  private int header_record_cnt = 0;
  private int detail_record_cnt = 0;
  private long posHeader = 0;
  private long posBody = 0;
  private long posAll = 0;
  private boolean isNewHeader = true;
  private ZKPositionTracker tracker;

  public static final String OUT_CHARSET_KEY = "outputCharset";
  public static final String CHARSET_DFLT = "UTF-8";

  public static final String TYPE_KEY = "packetType";
  public static final int TYPE_DFLT = 0;
  public static final String H_LEN_KEY = "packetLen0";
  public static final String B_LEN_KEY = "packetLen1";
  public static final int LEN_DFLT = 0;
    

  ByteUDRDeserializer(Context context, ResettableInputStream in) {
    this.in = in;
    if(context==null) logger.debug("[JHK] context is null");
    else {
    	logger.debug("[JHK] TYPE_CDR_UDR"+context.getString("TRACKER.TYPE_CDR_UDR", "0"));
    	logger.debug("[JHK] ZK_PATH_KEY"+context.getString("TRACKER.ZK_PATH_KEY", "0"));
    	logger.debug("[JHK] TARGET"+context.getString("TRACKER.TARGET", "0"));
    	logger.debug("[JHK] ZK_HOST_PORTS"+context.getString("TRACKER.ZK_HOST_PORTS", "0"));
    }
    this.tracker = ZKPositionTracker.getInstance(context.getInteger("TRACKER.TYPE_CDR_UDR", 0)
    											,context.getString("TRACKER.ZK_PATH_KEY")
    											,context.getString("TRACKER.TARGET")
    											,context.getString("TRACKER.ZK_HOST_PORTS","localhost:2181"));

    this.outputCharset = Charset.forName(
        context.getString(OUT_CHARSET_KEY, CHARSET_DFLT));
    this.packetType = context.getInteger(TYPE_KEY, TYPE_DFLT);
    this.headPacketLength = context.getInteger(H_LEN_KEY, LEN_DFLT);
    this.bodyPacketLength = context.getInteger(packetType==1?B_LEN_KEY:H_LEN_KEY, LEN_DFLT);
    this.isOpen = true;
    this.header_record_cnt = 0;
    this.detail_record_cnt = 0;
    this.isNewHeader = true;
    this.byteHeader = new byte[headPacketLength];
    this.byteBody = new byte[bodyPacketLength];
    this.byteEvent = new byte[headPacketLength+bodyPacketLength];
    
    this.posAll = tracker.getPosition();
    this.posBody = tracker.getPosition(0);
    this.posHeader = tracker.getPosition(1);
    
    if(this.posAll>0) {
    	try {
    		reReadHeaderEvent(posHeader);
    		in.seek(posAll);
    	} catch (IOException e) {
    		e.printStackTrace();
    	}
    }
  }

  /**
   * Reads a record from a file and returns an event
   * @return Event containing parsed record
   * @throws IOException
   */
 //Override
  public Event readEvent() throws IOException {
    ensureOpen();
    //ByteArrayOutputStream baos = null;
    byte[] buf = new byte[Math.max(headPacketLength, bodyPacketLength)];
    int n = 0;
    
    n = readCDRRecord(buf);
    
    if(n != -1 && isNewHeader && n==headPacketLength) {
    	header_record_cnt++;
    	System.arraycopy(buf, 0, byteEvent, 0, headPacketLength);
    	logger.debug("[JHK] header pos: before({}), after({})", tracker.getPosition(1), tracker.getPosition());
    	this.posAll += n;
    	this.posHeader += n;
    	// 여기에서 위치 등록은 소스데이터가 채널 프로세스에서 정상 처리여부를 확인 할 수 없다.
    	// 따라서 위치 정보를 채널 프로세스로 전달 할 수 있는 메커니즘을 정의하고 활용되도록 준비해야 함.
    	//tracker.storePosition(1, tracker.getPosition());
    	n = readCDRRecord(buf);
    }
    
    if(n != -1 && n==bodyPacketLength) {
    	detail_record_cnt++;
    	System.arraycopy(buf, 0, byteEvent, headPacketLength, bodyPacketLength);
    	logger.debug("[JHK] header pos: before({}), after({})", tracker.getPosition(0), tracker.getPosition());
    	this.posAll += n;
    	this.posBody = this.posAll;
    	// 여기에서 위치 등록은 소스데이터가 채널 프로세스에서 정상 처리여부를 확인 할 수 없다.
    	// 따라서 위치 정보를 채널 프로세스로 전달 할 수 있는 메커니즘을 정의하고 활용되도록 준비해야 함.
    	//tracker.storePosition(0, tracker.getPosition());
    }

    if (n==-1) {
    	logger.info("<<<Read Records>>>> : {} headers && {} bodys", Integer.toString(header_record_cnt), Integer.toString(detail_record_cnt));
    	return null; 
    } else if(n != bodyPacketLength) {
    	logger.debug("<<<JHK !!! ERROR>>>> : Data length error: we wish {} but get {} ", Integer.toString(bodyPacketLength), Integer.toString(n));
    	return null;
    	
    } else {
    	//System.arraycopy(byteHeader, 0, byteEvent, 0, headPacketLength);
    	//System.arraycopy(byteBody, 0, byteEvent, headPacketLength, bodyPacketLength);
    	
    	return EventBuilder.withBody(byteEvent);
    }
  }
  

  public void reReadHeaderEvent(long pos) throws IOException {
    ensureOpen();
    byte[] buf = new byte[Math.max(headPacketLength, bodyPacketLength)];
    int n = 0;
    
    in.seek(pos-headPacketLength);
    
    n = readCDRRecord(buf);
    
    if(n != -1 && isNewHeader && n==headPacketLength) {
    	header_record_cnt++;
    	System.arraycopy(buf, 0, byteEvent, 0, headPacketLength);
    } else {
    	logger.error("You must re-read the header data. but can't");
    	throw new IOException("You must re-read the header data. but can't");
    }
  }
  
  public long getPos(int type) {
	  if(type==0) {
		  return posBody;
	  } else if(type==1) {
		  return posHeader;
	  } else {
		  return posAll;
	  }
  }

  public int readCDRRecord(byte[] buf) throws IOException {
	int n = 0;
    int readCnt = 0;
    
    /*
    ByteArrayOutputStream baos = null;
	byte[] tmpBuf = new byte[buf.length];
	n=in.read(tmpBuf,0,1);
    readCnt += n;
    if(n != -1 && tmpBuf[0]==0x30) {
    	isNewHeader = true;
    	while(n != -1 && readCnt<headPacketLength) {
        	n = in.read(tmpBuf,0,headPacketLength-readCnt);
            if(n != -1) {
            	readCnt += n;
                if(baos == null) {
                	baos = new ByteArrayOutputStream(n);
                }
                baos.write(tmpBuf, 0, n);
            }
    	}
    } else if(n != -1 && tmpBuf[0]==0x31) {
    	isNewHeader = false;
    	while(n != -1 && readCnt<bodyPacketLength) {
        	n = in.read(tmpBuf,0,bodyPacketLength-readCnt);
            if(n != -1) {
            	readCnt += n;
                if(baos == null) {
                	baos = new ByteArrayOutputStream(n);
                }
                baos.write(tmpBuf, 0, n);
            }
    	}
    }
    byte[] tb = baos.toByteArray();
    System.arraycopy(tb, 0, buf, 0, isNewHeader?headPacketLength:bodyPacketLength);
	return readCnt;
	**/
    
    n = in.read(buf,0,1);
    readCnt += n;
    if(n != -1 && buf[0]==0x30) {
    	isNewHeader = true;
    	while(n != -1 && readCnt<headPacketLength) {
        	n = in.read(buf,readCnt,headPacketLength-readCnt);
            readCnt += n;
    	}
    } else if(n != -1 && buf[0]==0x31) {
    	isNewHeader = false;
    	while(n != -1 && readCnt<bodyPacketLength) {
        	n = in.read(buf,readCnt,bodyPacketLength-readCnt);
            readCnt += n;
    	}
    }
	return readCnt; 
  }

  /**
   * Batch record read
   * @param numEvents Maximum number of events to return.
   * @return List of events containing read records
   * @throws IOException
   */
  //Override
  public List<Event> readEvents(int numEvents) throws IOException {
    ensureOpen();
    List<Event> events = Lists.newLinkedList();
    for (int i = 0; i < numEvents; i++) {
      Event event = readEvent();
      
      ///////////////////////////
      /* for debugging
      if(detail_record_cnt>=2000) {
      	logger.warn("[detail_record_cnt>=2000] : {} headers && {} bodys", Integer.toString(header_record_cnt), Integer.toString(detail_record_cnt));
      	System.exit(0);
      } else if(detail_record_cnt % 200 == 0) {
        	logger.warn("[detail_record_cnt % 200 == 0] : {} headers && {} bodys", Integer.toString(header_record_cnt), Integer.toString(detail_record_cnt));
          	//System.exit(0);
      }
      */
      /////////////////////////
      
      if (event != null) {
        events.add(event);
      } else {
        break;
      }
    }
    return events;
  }

  //Override
  public void mark() throws IOException {
    ensureOpen();
    /*
    logger.debug("[JHK] className: {}, Method:mark()", this.getClass().getName());
    try {
    	in.mark();
    } catch(IOException ioe) {
    	ioe.printStackTrace();
    	throw ioe;
    }
    */
    try {
    	tracker.update("/pos", Long.toString(posAll).getBytes());
    	tracker.update("/pos/b", Long.toString(posBody).getBytes());
    	tracker.update("/pos/h", Long.toString(posHeader).getBytes());
    } catch(Exception e) {
    	e.printStackTrace();
    	throw new IOException(e);
    }
    
  }

  //Override
  public void reset() throws IOException {
    ensureOpen();
    logger.debug("[JHK] className: {}, Method:reset()", this.getClass().getName());
    try {
    	in.reset();
    } catch(IOException ioe) {
    	ioe.printStackTrace();
    	throw ioe;
    }
  }

  //Override
  public void close() throws IOException {
    if (isOpen) {
      reset();
      in.close();
      isOpen = false;
    }
  }

  private void ensureOpen() {
    if (!isOpen) {
      throw new IllegalStateException("Serializer has been closed");
    }
  }



  public static class Builder implements EventDeserializer.Builder {

    //Override
    public EventDeserializer build(Context context, ResettableInputStream in) {
        return new ByteUDRDeserializer(context, in);
      }
  }

}
