/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.jhk.cdr.flume;

import org.apache.flume.serialization.*;
import com.google.common.collect.Lists;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.annotations.InterfaceAudience;
import org.apache.flume.annotations.InterfaceStability;
import org.apache.flume.event.EventBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

/**
 * A deserializer that parses CDR Records from a file.
 */
@InterfaceAudience.Private
@InterfaceStability.Evolving
public class CDRDeserializer implements EventDeserializer {

  private static final Logger logger = LoggerFactory.getLogger(CDRDeserializer.class);

  private final ResettableInputStream in;
  private final Charset outputCharset;
  //private final int packetType;
  //private final int headPacketLength;
  private final int bodyPacketLength;
  private volatile boolean isOpen;
  //private String strHeader = null;
  private String strBody = null;
  //private int header_record_cnt = 0;
  private int detail_record_cnt = 0;
  //private boolean isNewHeader = true;

  public static final String OUT_CHARSET_KEY = "outputCharset";
  public static final String CHARSET_DFLT = "UTF-8";

  public static final String TYPE_KEY = "packetType";
  public static final int TYPE_DFLT = 0;
  public static final String LEN_KEY = "packetLen0";
  public static final int LEN_DFLT = 0;
  
  
  CDRDeserializer(Context context, ResettableInputStream in) {
    this.in = in;
    this.outputCharset = Charset.forName(
        context.getString(OUT_CHARSET_KEY, CHARSET_DFLT));
    //this.packetType = context.getInteger(TYPE_KEY, TYPE_DFLT);
    //this.headPacketLength = context.getInteger(H_LEN_KEY, LEN_DFLT);
    this.bodyPacketLength = context.getInteger(LEN_KEY, LEN_DFLT);
    this.isOpen = true;
    //this.header_record_cnt = 0;
    this.detail_record_cnt = 0;
    //this.isNewHeader = packetType==1?true:false;;
  }

  /**
   * Reads a record from a file and returns an event
   * @return Event containing parsed record
   * @throws IOException
   */
 //Override
  public Event readEvent() throws IOException {
    ensureOpen();
    String record = null;
    record = readCDRRecord();
    
    if (record == null) {
    	logger.warn("<<<Read Records>>>> : {} ", Integer.toString(detail_record_cnt));
    	return null;
    } else {
    	detail_record_cnt++;
    	return EventBuilder.withBody(record, outputCharset);
    }
  }

  /**
   * Batch record read
   * @param numEvents Maximum number of events to return.
   * @return List of events containing read records
   * @throws IOException
   */
  //Override
  public List<Event> readEvents(int numEvents) throws IOException {
    ensureOpen();
    List<Event> events = Lists.newLinkedList();
    for (int i = 0; i < numEvents; i++) {
      Event event = readEvent();
      if (event != null) {
        events.add(event);
      } else {
        break;
      }
    }
    return events;
  }

  //Override
  public void mark() throws IOException {
    ensureOpen();
    in.mark();
  }

  //Override
  public void reset() throws IOException {
    ensureOpen();
    in.reset();
  }

  //Override
  public void close() throws IOException {
    if (isOpen) {
      reset();
      in.close();
      isOpen = false;
    }
  }

  private void ensureOpen() {
    if (!isOpen) {
      throw new IllegalStateException("Serializer has been closed");
    }
  }

  private byte myRead() throws IOException {
	  byte[] b = new byte[1];
	  in.read(b,0,1);
	  return b[0];
  }
  
  // TODO: consider not returning a final character that is a high surrogate
  // when truncating
  private String readCDRRecord() throws IOException {
    StringBuilder sb = new StringBuilder();
    int c;
    byte[] b = new byte[1];
    int readChars = 0;
    
    while ((c = myRead()) != -1) {
	      readChars++;
	      sb.append((char)c);

	      if (readChars >= bodyPacketLength) {
	        break;
	      }    	
    }

    if (readChars != bodyPacketLength) {
    	logger.warn("Record length is wrong({}), truncating record!!", bodyPacketLength);
    }
    if (readChars > 0) {
      return sb.toString();
    } else {
      return null;
    }
  }

  public static class Builder implements EventDeserializer.Builder {

    //Override
    public EventDeserializer build(Context context, ResettableInputStream in) {
      return new CDRDeserializer(context, in);
    }

  }

}
