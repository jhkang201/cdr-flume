package dev.jhk.cdr.flume;

import org.apache.flume.serialization.*;
import com.google.common.collect.Lists;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.annotations.InterfaceAudience;
import org.apache.flume.annotations.InterfaceStability;
import org.apache.flume.event.EventBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

/**
 * A deserializer that parses CDR Records from a file.
 */
@InterfaceAudience.Private
@InterfaceStability.Evolving
public class ByteCDRDeserializer implements EventDeserializer {

  private static final Logger logger = LoggerFactory.getLogger(ByteCDRDeserializer.class);

  private final ResettableInputStream in;
  private final Charset outputCharset;
  private final int bodyPacketLength;
  private volatile boolean isOpen;
  private int header_record_cnt = 0;
  private int detail_record_cnt = 0;

  public static final String OUT_CHARSET_KEY = "outputCharset";
  public static final String CHARSET_DFLT = "UTF-8";

  public static final int TYPE_DFLT = 0;
  public static final String H_LEN_KEY = "packetLen0";
  public static final int LEN_DFLT = 0;
  

  ByteCDRDeserializer(Context context, ResettableInputStream in) {
    this.in = in;
    this.outputCharset = Charset.forName(
        context.getString(OUT_CHARSET_KEY, CHARSET_DFLT));
    this.bodyPacketLength = context.getInteger(H_LEN_KEY, LEN_DFLT);
    this.isOpen = true;
    this.header_record_cnt = 0;
    this.detail_record_cnt = 0;
  }

  /**
   * Reads a record from a file and returns an event
   * @return Event containing parsed record
   * @throws IOException
   */
 //Override
  public Event readEvent() throws IOException {
    ensureOpen();
    byte[] buf = new byte[bodyPacketLength];
    int n = 0;
    
    n = in.read(buf, 0, bodyPacketLength);
    if (n==-1) {
    	return null;
    } else {
    	if(n<bodyPacketLength) {
    		logger.warn("Packet length is wrong: we wish {} but get {} !!",Integer.toString(bodyPacketLength), Integer.toString(n));
    	}
    	return EventBuilder.withBody(buf);
    }
  }

  /**
   * Batch record read
   * @param numEvents Maximum number of events to return.
   * @return List of events containing read records
   * @throws IOException
   */
  //Override
  public List<Event> readEvents(int numEvents) throws IOException {
    ensureOpen();
    List<Event> events = Lists.newLinkedList();
    for (int i = 0; i < numEvents; i++) {
      Event event = readEvent();
      if (event != null) {
        events.add(event);
      } else {
        break;
      }
    }
    return events;
  }

  //Override
  public void mark() throws IOException {
    ensureOpen();
    in.mark();
  }

  //Override
  public void reset() throws IOException {
    ensureOpen();
    in.reset();
  }

  //Override
  public void close() throws IOException {
    if (isOpen) {
      reset();
      in.close();
      isOpen = false;
    }
  }

  private void ensureOpen() {
    if (!isOpen) {
      throw new IllegalStateException("Serializer has been closed");
    }
  }

  public static class Builder implements EventDeserializer.Builder {

    //Override
    public EventDeserializer build(Context context, ResettableInputStream in) {
      return new ByteCDRDeserializer(context, in);
    }

  }

}
