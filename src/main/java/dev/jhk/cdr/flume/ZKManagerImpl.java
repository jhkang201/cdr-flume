package dev.jhk.cdr.flume;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ZKManagerImpl implements ZKManager {
	  private static final Logger logger = LoggerFactory.getLogger(ZKManagerImpl.class);
    private static ZooKeeper zkeeper;
    private static ZKConnection zkConnection;

    public ZKManagerImpl(String zookeeperHostPorts) {
        initialize(zookeeperHostPorts);
    }

    /** * Initialize connection */
    private void initialize(String zookeeperHostPorts) {
        if(zkeeper != null) return;
        
    	try {
            zkConnection = new ZKConnection();
            zkeeper = zkConnection.connect(zookeeperHostPorts);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    public void closeConnection() {
        try {
            zkConnection.close();
        } catch (InterruptedException e) {
        	logger.error(e.getMessage());
        }
    }

    public void create(String path, byte[] data) throws KeeperException, InterruptedException {
        zkeeper.create(path, data, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);
    }

    public String getZNodeData(String path, boolean watchFlag) {
        try {
            byte[] b = null;
            b = zkeeper.getData(path, null, null);
            String data = new String(b, "UTF-8");
            logger.debug(data);
            return data;
        } catch (Exception e) {
        	logger.error(e.getMessage());
        }
        return null;
    }

    public void update(String path, byte[] data) throws KeeperException, InterruptedException {
        int version = -1;
        try {
        	version = zkeeper.exists(path, true).getVersion();
        } catch(Exception e) {
        	version = -1;
        }
        zkeeper.setData(path, data, version);
    }
}