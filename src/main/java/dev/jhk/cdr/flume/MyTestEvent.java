package dev.jhk.cdr.flume;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.flume.Event;

public class MyTestEvent implements Event, Serializable {

	private static final long serialVersionUID = 1L;
	
	// custom keys of your json schema 
	private String my_event_property1;
	private String my_event_property2;

	private transient String charset = "UTF-8";
	private Map<String, String> headers = new HashMap<String, String>();

	public Map<String, String> getHeaders() {
		return this.headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public byte[] getBody() {
		
		try {
			return getByteRepresentation();
		} catch (Exception e) {
			return new byte[0];
		}
	}

	public void setBody(byte[] body) {

	}
	
	public void setCharset(String charset) {
		this.charset = charset;
	}
	
	private byte[] getByteRepresentation() {
		return SerializationUtils.serialize(this);
	}
	
}