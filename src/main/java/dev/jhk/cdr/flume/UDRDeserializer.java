/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package dev.jhk.cdr.flume;

import org.apache.flume.serialization.*;
import com.google.common.collect.Lists;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.annotations.InterfaceAudience;
import org.apache.flume.annotations.InterfaceStability;
import org.apache.flume.event.EventBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

/**
 * A deserializer that parses UDR Records from a file.
 */
@InterfaceAudience.Private
@InterfaceStability.Evolving
public class UDRDeserializer implements EventDeserializer {

  private static final Logger logger = LoggerFactory.getLogger(UDRDeserializer.class);

  private final ResettableInputStream in;
  private final Charset outputCharset;
  private final int packetType;
  private final int headPacketLength;
  private final int bodyPacketLength;
  private volatile boolean isOpen;
  private String strHeader = null;
  private String strBody = null;
  private byte[] byteHeader = null;
  private byte[] byteBody = null;
  private int header_record_cnt = 0;
  private int detail_record_cnt = 0;
  private boolean isNewHeader = true;

  public static final String OUT_CHARSET_KEY = "outputCharset";
  public static final String CHARSET_DFLT = "UTF-8";

  public static final String TYPE_KEY = "packetType";
  public static final int TYPE_DFLT = 0;
  public static final String H_LEN_KEY = "packetLen0";
  public static final String B_LEN_KEY = "packetLen1";
  public static final int LEN_DFLT = 0;
  
  
  UDRDeserializer(Context context, ResettableInputStream in) {
    this.in = in;
    this.outputCharset = Charset.forName(
        context.getString(OUT_CHARSET_KEY, CHARSET_DFLT));
    this.packetType = context.getInteger(TYPE_KEY, TYPE_DFLT);
    this.headPacketLength = context.getInteger(H_LEN_KEY, LEN_DFLT);
    this.bodyPacketLength = context.getInteger(B_LEN_KEY, LEN_DFLT);
    this.isOpen = true;
    this.header_record_cnt = 0;
    this.detail_record_cnt = 0;
    this.isNewHeader = true;
    this.byteHeader = new byte[this.headPacketLength];
    this.byteBody = new byte[this.bodyPacketLength];
  }

  /**
   * Reads a record from a file and returns an event
   * @return Event containing parsed record
   * @throws IOException
   */
 //Override
  public Event readEvent() throws IOException {
	    ensureOpen();
	    byte[] record = null;
	    //byte[] byteForEvent = new byte[(isNewHeader?headPacketLength:bodyPacketLength)+2];
	    
	    record = readCDRRecord();
	    if(isNewHeader) {
	    	header_record_cnt++;
	    	//byteForEvent[0]= '[';
	    	//byteForEvent[(isNewHeader?headPacketLength:bodyPacketLength)+1]=']';
	    } else {
	    	detail_record_cnt++;
	    	//byteForEvent[0]= '<';
	    	//byteForEvent[(isNewHeader?headPacketLength:bodyPacketLength)+1]='>';
	    }

	    if (record == null) {
	    	logger.warn("<<<Read Records>>>> : {} headers && {} bodys", Integer.toString(header_record_cnt), Integer.toString(detail_record_cnt));
	    	return null;
	    } else {
	    	//System.arraycopy(record, 0, byteForEvent, 1, byteForEvent.length-2);
	    	return EventBuilder.withBody(record);
	    }
	  }
  /*
  public Event readEvent() throws IOException {
    ensureOpen();
    String record = null;
    record = readCDRRecord();
    if(isNewHeader) {
    	strHeader = record;
    	header_record_cnt++;
    	record = readCDRRecord();
    } 

    if (record == null) {
    	logger.warn("<<<Read Records>>>> : {} headers && {} bodys", Integer.toString(header_record_cnt), Integer.toString(detail_record_cnt));
    	return null;
    } else {
    	detail_record_cnt++;
    	return EventBuilder.withBody(strHeader+record, outputCharset);
    }
  }
  **/

  /**
   * Batch record read
   * @param numEvents Maximum number of events to return.
   * @return List of events containing read records
   * @throws IOException
   */
  //Override
  public List<Event> readEvents(int numEvents) throws IOException {
    ensureOpen();
    List<Event> events = Lists.newLinkedList();
    for (int i = 0; i < numEvents; i++) {
      Event event = readEvent();
      if (event != null) {
        events.add(event);
      } else {
        break;
      }
    }
    return events;
  }

  //Override
  public void mark() throws IOException {
    ensureOpen();
    in.mark();
  }

  //Override
  public void reset() throws IOException {
    ensureOpen();
    in.reset();
  }

  //Override
  public void close() throws IOException {
    if (isOpen) {
      reset();
      in.close();
      isOpen = false;
    }
  }

  private void ensureOpen() {
    if (!isOpen) {
      throw new IllegalStateException("Serializer has been closed");
    }
  }

  private byte myRead() throws IOException {
	  byte[] b = new byte[1];
	  in.read(b,0,1);
	  return b[0];
  }
  
  // TODO: consider not returning a final character that is a high surrogate
  // when truncating
  private byte[] readCDRRecord() throws IOException {
    StringBuilder sb = new StringBuilder();
    byte c;
    byte[] b = null;
    int readChars = 0;
    
    //if((c = in.read()) == '0') {
    if((c = myRead()) == '0') {
    	isNewHeader = true;
    	b = new byte[headPacketLength];
        do {
        	b[readChars++] = c;
	        if (readChars >= headPacketLength) {
	            break;
	            }
        //} while ((c = in.read()) != -1) ;        
        } while ((c = myRead()) != -1) ;
    	
    } else if (c != -1) {
    	isNewHeader = false;
    	b = new byte[bodyPacketLength];
    	if(c != '1') {
        	//logger.warn("[JHK:{}] Body type ERROR@ {} headers && {} bodys", Byte.toString(c), Integer.toString(header_record_cnt), Integer.toString(detail_record_cnt));
        	logger.warn("[JHK:{}] Body type ERROR@  {} bodys", Byte.toString(c), Integer.toString(detail_record_cnt));
    	}
    	do {
        	b[readChars++] = c;

    	      if (readChars >= bodyPacketLength) {
    	        break;
    	      }
    	//} while ((c = in.read()) != -1);
    	} while ((c = myRead()) != -1);
    }

    if (readChars != (isNewHeader?headPacketLength:bodyPacketLength)) {
    	logger.warn("{} Record length is wrong({}), truncating record!!", (isNewHeader?"Head":"Body"), (isNewHeader?headPacketLength:bodyPacketLength));
    }
    if (readChars > 0) {
      return b;
    } else {
      return null;
    }
  }

  public static class Builder implements EventDeserializer.Builder {

    //Override
    public EventDeserializer build(Context context, ResettableInputStream in) {
      return new UDRDeserializer(context, in);
    }

  }

}
