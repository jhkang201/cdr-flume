package dev.jhk.cdr.flume;

import org.apache.flume.serialization.*;
import com.google.common.collect.Lists;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.annotations.InterfaceAudience;
import org.apache.flume.annotations.InterfaceStability;
import org.apache.flume.event.EventBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;

/**
 * A deserializer that parses UDR Records from a file.
 */
@InterfaceAudience.Private
@InterfaceStability.Evolving
public class CharUDRDeserializer implements EventDeserializer {

  private static final Logger logger = LoggerFactory.getLogger(CharUDRDeserializer.class);

  private final ResettableInputStream in;
  private final Charset outputCharset;
  private final int packetType;
  private final int headPacketLength;
  private final int bodyPacketLength;
  private volatile boolean isOpen;
  private String strHeader = null;
  private int header_record_cnt = 0;
  private int detail_record_cnt = 0;
  private boolean isNewHeader = true;

  public static final String OUT_CHARSET_KEY = "outputCharset";
  public static final String CHARSET_DFLT = "UTF-8";

  public static final String TYPE_KEY = "packetType";
  public static final int TYPE_DFLT = 0;
  public static final String H_LEN_KEY = "packetLen0";
  public static final String B_LEN_KEY = "packetLen1";
  public static final int LEN_DFLT = 0;
  

  CharUDRDeserializer(Context context, ResettableInputStream in) {
    this.in = in;
    this.outputCharset = Charset.forName(
        context.getString(OUT_CHARSET_KEY, CHARSET_DFLT));
    this.packetType = context.getInteger(TYPE_KEY, TYPE_DFLT);
    this.headPacketLength = context.getInteger(H_LEN_KEY, LEN_DFLT);
    this.bodyPacketLength = context.getInteger(packetType==1?B_LEN_KEY:H_LEN_KEY, LEN_DFLT);
    this.isOpen = true;
    this.header_record_cnt = 0;
    this.detail_record_cnt = 0;
    this.isNewHeader = true;
  }

  /**
   * Reads a record from a file and returns an event
   * @return Event containing parsed record
   * @throws IOException
   */
 //Override
  public Event readEvent() throws IOException {
    ensureOpen();
    String record = null;
    record = readCDRRecord();
    if(isNewHeader) {
    	strHeader = record;
    	header_record_cnt++;
    	record = readCDRRecord();
    } 

    if (record == null) {
    	logger.warn("<<<Read Records>>>> : {} headers && {} bodys", Integer.toString(header_record_cnt), Integer.toString(detail_record_cnt));
    	return null;
    } else {
    	detail_record_cnt++;
    	return EventBuilder.withBody(strHeader+record, outputCharset);
    }
  }

  /**
   * Batch record read
   * @param numEvents Maximum number of events to return.
   * @return List of events containing read records
   * @throws IOException
   */
  //Override
  public List<Event> readEvents(int numEvents) throws IOException {
    ensureOpen();
    List<Event> events = Lists.newLinkedList();
    for (int i = 0; i < numEvents; i++) {
      Event event = readEvent();
      if (event != null) {
        events.add(event);
      } else {
        break;
      }
    }
    return events;
  }

  //Override
  public void mark() throws IOException {
    ensureOpen();
    in.mark();
  }

  //Override
  public void reset() throws IOException {
    ensureOpen();
    in.reset();
  }

  //Override
  public void close() throws IOException {
    if (isOpen) {
      reset();
      in.close();
      isOpen = false;
    }
  }

  private void ensureOpen() {
    if (!isOpen) {
      throw new IllegalStateException("Serializer has been closed");
    }
  }

  // TODO: consider not returning a final character that is a high surrogate
  // when truncating
  private String readCDRRecord() throws IOException {
    StringBuilder sb = new StringBuilder();
    int c;
    int readChars = 0;
    
    if((c = in.readChar()) == '0') {
    	isNewHeader = true;
        do {
	    	readChars++;
	        sb.append((char)c);
	        if (readChars >= headPacketLength) {
	            break;
	            }
        
        } while ((c = in.readChar()) != -1) ;
    	
    } else if (c != -1) {
    	isNewHeader = false;
    	do {
    	      readChars++;
    	      sb.append((char)c);

    	      if (readChars >= bodyPacketLength) {
    	        break;
    	      }
    		
    	} while ((c = in.readChar()) != -1);
    }

    if (readChars != (isNewHeader?headPacketLength:bodyPacketLength)) {
    	logger.warn("{} Record length is wrong({}), truncating record!!", (isNewHeader?"Head":"Body"), (isNewHeader?headPacketLength:bodyPacketLength));
    }
    if (readChars > 0) {
      return sb.toString();
    } else {
      return null;
    }
  }

  public static class Builder implements EventDeserializer.Builder {

    //Override
    public EventDeserializer build(Context context, ResettableInputStream in) {
      return new CharUDRDeserializer(context, in);
    }

  }

}
