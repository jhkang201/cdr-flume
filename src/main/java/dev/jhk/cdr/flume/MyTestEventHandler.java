package dev.jhk.cdr.flume;

import java.io.BufferedReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.source.http.HTTPBadRequestException;
import org.apache.flume.source.http.HTTPSourceHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;


public class MyTestEventHandler implements HTTPSourceHandler {

	private static final Logger LOG = LoggerFactory.getLogger(MyTestEventHandler.class);
	private final Type listType = new TypeToken<List<MyTestEvent>>(){}.getType();
	private Gson gson;
	
	public void TrackingEventHandler() {
		gson = new GsonBuilder().disableHtmlEscaping().create();
	}
	
	//Override
	public void configure(Context context) {
	}

	//Override
	public List<Event> getEvents(HttpServletRequest request) throws HTTPBadRequestException, Exception {
		
		BufferedReader reader = request.getReader();
		String charset = request.getCharacterEncoding();

		List<Event> eventList = new ArrayList<Event>(0);
		
		try {
			eventList = gson.fromJson(reader, listType);
		} catch (JsonSyntaxException ex) {
			throw new HTTPBadRequestException("Request has invalid JSON Syntax.", ex);
		}

		for (Event e : eventList) {
			((MyTestEvent) e).setCharset(charset);
		}
		
		return eventList;
	}

}