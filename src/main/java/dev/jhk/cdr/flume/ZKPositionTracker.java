package dev.jhk.cdr.flume;

import java.io.File;
import java.io.IOException;

import com.google.common.base.Preconditions;
/*
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;
*/
import org.apache.flume.annotations.InterfaceAudience;
import org.apache.flume.annotations.InterfaceStability;
import org.apache.flume.tools.PlatformDetect;
import org.apache.zookeeper.KeeperException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.flume.serialization.PositionTracker;

@InterfaceAudience.Private
@InterfaceStability.Evolving
public class ZKPositionTracker implements PositionTracker {
	  private static final Logger logger = LoggerFactory
		      .getLogger(ZKPositionTracker.class);

  public static enum PacketType {
	  CDR, UDR
  }
  
  private static ZKManagerImpl zkManager;
  private String target;
  private String zkPath;
  long targetPosition;
  long targetBodyPosition;
  long targetHeadPosition;
  private boolean isOpen;
  private boolean completed;

  /**
   * If the file exists at startup, then read it, roll it, and open a new one.
   * We go through this to avoid issues with partial reads at the end of the
   * file from a previous crash. If we append to a bad record,
   * our writes may never be visible.
   * @param pathKey
   * @param target
   * @return
   */
  public static ZKPositionTracker getInstance(int type, String pathKey, String target, String zkHosts) {
    
	return new ZKPositionTracker(type, pathKey, target, zkHosts);
  }


  /**
   * If the file exists, read it and open it for append.
   * @param type
   * @param pathKey
   * @param target
   * @param zkHosts
   */
  ZKPositionTracker(int type, String pathKey, String target, String zkHosts)  {

    Preconditions.checkNotNull(pathKey, "pathKey must not be null");
    //Preconditions.checkNotNull(target, "target must not be null");

    this.zkPath = "/tracker"
			/*+ "/" + (type==ZKPositionTracker.PacketType.CDR ? "CDR" : "UDR")*/
			+ "/" + (type==0 ? "CDR" : "UDR")
			+ "/" + pathKey;
    this.target = target;
    
    zkManager = new ZKManagerImpl(zkHosts);
	String existingTarget = (String)zkManager.getZNodeData(zkPath+"/target", true);
	if(target == null) target = existingTarget;
	
	String pos = zkManager.getZNodeData(zkPath+"/pos", true);
	String bPos = zkManager.getZNodeData(zkPath+"/pos/b", true);
	String hPos = zkManager.getZNodeData(zkPath+"/pos/h", true);
	targetPosition = pos==null?-1:Long.parseLong(pos);
	targetBodyPosition = bPos==null?-1:Long.parseLong(bPos);
	targetHeadPosition = type==0 //type==PacketType.CDR 
							? -1 
							: hPos==null?-1:Long.parseLong(hPos);
	completed = (String)zkManager.getZNodeData(zkPath+"/completed", true) == "TRUE";
    if(!completed && !target.equals(existingTarget)) {
    	logger.error("Target ERROR: completed : ", zkManager.getZNodeData(zkPath+"/completed", true));
    	logger.error("Target ERROR: we wish {}, but {}", target, existingTarget);
    	logger.error("Target ERROR: head {}, body {}", targetPosition, targetHeadPosition);
    	targetPosition = 0;
    	targetBodyPosition = 0;
    	targetHeadPosition = 0;
    	try {
    		zkManager.update(zkPath+"/target", target.getBytes());
    		zkManager.update(zkPath+"/pos", Long.toString(targetPosition).getBytes());
    		zkManager.update(zkPath+"/pos/b", Long.toString(targetBodyPosition).getBytes());
    		if(type==1) {
    			zkManager.update(zkPath+"/pos/h", Long.toString(targetHeadPosition).getBytes());
    		}
    		zkManager.update(zkPath+"/completed", "FALSE".getBytes());
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    } else if (completed && !target.equals(existingTarget)) {
    	targetPosition = 0;
    	targetBodyPosition = 0;
    	targetHeadPosition = 0;
    	try {
    		zkManager.update(zkPath+"/target", target.getBytes());
    		zkManager.update(zkPath+"/pos", Long.toString(targetPosition).getBytes());
    		zkManager.update(zkPath+"/pos/b", Long.toString(targetBodyPosition).getBytes());
    		if(type==1) {
    			zkManager.update(zkPath+"/pos/h", Long.toString(targetHeadPosition).getBytes());
    		}
    		zkManager.update(zkPath+"/completed", "FALSE".getBytes());
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    }
	isOpen = true;
  }



  //@Override
  public synchronized void storePosition(long position) throws IOException {
	  //*
	  try {
		  zkManager.update(zkPath+"/pos", Long.toString(position).getBytes());
	  } catch(Exception e) {
		  e.printStackTrace();
		  throw new IOException(e);
	  }
	  //*/ throw new IOException("[JHK] >>>>>>>>>>>>>>>>>>>>");
  }
  
  public synchronized void storePosition(int head_or_body, long position) throws IOException {
	  try {
		  if(head_or_body==0) { //body(0), head(1)
			  zkManager.update(zkPath+"/pos/b", Long.toString(position).getBytes());
		  } else {
			  zkManager.update(zkPath+"/pos/h", Long.toString(position).getBytes());
		  }
	  } catch(Exception e) {
		  e.printStackTrace();
		  throw new IOException(e);
	  }
  }

  //@Override
  public synchronized long getPosition() {
	  long lval = -1;
	  try {
		  lval = Long.parseLong((String)zkManager.getZNodeData(zkPath+"/pos", true));
	  } catch (Exception e) {
		  e.printStackTrace();
	  }
	  return lval;
  }
  
  public synchronized long getPosition(int head_or_body) {
	  long lval = -1;
	  try {
		  if(head_or_body==0) { //body(0), head(1)
			  lval = Long.parseLong((String)zkManager.getZNodeData(zkPath+"/pos/b", true));
		  } else {
			  lval = Long.parseLong((String)zkManager.getZNodeData(zkPath+"/pos/h", true));
		  }
	  } catch (Exception e) {
		  e.printStackTrace();
	  }
	return lval;
  }

  //@Override
  public String getTarget() {
    return (String)zkManager.getZNodeData(zkPath+"/target", true);
  }

  //@Override
  public void close() throws IOException {
	  isOpen = false;
	  //zkManager.closeConnection();
  }

  public String getLastFileSeq() {
    return (String)zkManager.getZNodeData(zkPath+"/last_sequence", true);
  }

  public String getCompleted() {
    return (String)zkManager.getZNodeData(zkPath+"/completed", true);
  }

  public void update(String subPath, byte[] data) throws KeeperException, InterruptedException {
	  zkManager.update(zkPath + subPath,  data);
  }
}